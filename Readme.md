# Iniciar o Node.js
    npm init --yes

# Instalando o Cypres
    npm install cypress --force

# Quando baixar projeto em Cypress que não vai ter as pastas node_modules e package-lock.json,
# pois no .gitignore normalmente é informando para não enviar ao repositorio
    npm install

# Instalar o Cypress neste projeto baixado
    npx cypress install

# Se já instalado, necessita reinstalar com --force  "^12.17.4",
    npx cypress install --force 

# Buscar versão mais rescente
  npm i cypress@latest

# Verificar a instalação do cypress
    npx cypress verify

# Abrir o cypress
    npx cypress open    

# Plugin Cucumber
https://github.com/badeball/cypress-cucumber-preprocessor

## 01) Instalação do cucumber
npm install @badeball/cypress-cucumber-preprocessor

## 01.01) Instalação do cypress-esbuild-preprocessor
npm install @bahmutov/cypress-esbuild-preprocessor

## Configurações cypress.config.js
const { defineConfig }                   = require("cypress");
const createBundler                      = require("@bahmutov/cypress-esbuild-preprocessor");
const { addCucumberPreprocessorPlugin }  = require("@badeball/cypress-cucumber-preprocessor");
const { createEsbuildPlugin }            = require("@badeball/cypress-cucumber-preprocessor/esbuild");

module.exports = defineConfig({
  e2e: {
    specPattern: "**/*.feature",
    setupNodeEvents(on, config) {
      // implement node event listeners here
      addCucumberPreprocessorPlugin(on, config);
      on(
        "file:preprocessor",
        createBundler({
          plugins: [createEsbuildPlugin(config)],
        })
      );
      return config;
    },
  },
});

>> ver   >>>
https://github.com/badeball/cypress-cucumber-preprocessor#cypress-configuration


# Faker JS
https://fakerjs.dev/guide/

## Instalação
npm install @faker-js/faker --save-dev

## Importar para o arquivo de teste
import { faker } from '@faker-js/faker';
// or, if desiring a different locale
// import { fakerDE as faker } from '@faker-js/faker';

# Generate Password
https://www.npmjs.com/package/generate-password

## Instalação
npm install generate-password --save

## Gerando Password
var generator = require('generate-password');

var password = generator.generate({
	length: 10,
	numbers: true
});

// 'uEyMTw32v9'
console.log(password);


# Relatório com mochawasome
https://github.com/LironEr/cypress-mochawesome-reporter

## Setup install cypress-mochawesome-reporter
npm i --save-dev cypress-mochawesome-reporter

## Change cypress reporter & setup hooks cypress.config.js

## Add to cypress/support/e2e.js
import 'cypress-mochawesome-reporter/register';

## Run Cypress
npx cypress run


# Adicionar Projeto no GitLab
## Push an existing folder
git init
git config --global user.name "Regina Becker"
git remote add origin https://gitlab.com/thinaregina/cypress-bdd.git
git add .
git commit -m "Initial commit"
git push -u origin master

## Criar Branch
Criar Branch
git checkout -b feature/branch-nova-oficial
git add .
git commit -m "Initial commit"
git push
git push --set-upstream origin feature/branch-nova-oficial
git fetch exibe todo conteúdo do repositorio gitlab e demais branchs
git checkout nomeoutrabranch
No gitlab clicar no botão Create merge request
Clicar no texto em From feature/branch-nova-oficial into master -->>  Change branches
Clicar no botão Compare branches and continue
Caso desejar excluir a branch
No rodapé Deixar marcado Merge options: Delete source branch when merge request is accepted.
Clicar no botão Create merge request
Clicar no like
Clicar em Merge
git checkout master
git pull

# Repositório Github
git init
git add .
git commit -m "Disponibilizar Projeto para repositorio Github"
git branch -M master
git remote add origin https://github.com/reginabecker/Bootcamp-cypress-primeiro.git
git push -u origin master

Executar em outros Browser
npx cypress open
instalar na maquina outra versão de browser já exibe nas opções





